import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class machineUI {
    private JPanel root;
    private JTabbedPane tabbedPane1;
    private JTextField SUMTextField;
    private JButton CancelButton;
    private JButton CheckButton;
    private JButton BlendH;
    private JButton AmericanH;
    private JButton AmericanC;
    private JButton BlendC;
    private JButton CocoaH;
    private JButton CocoaC;
    private JButton TeaH;
    private JButton TeaC;
    private JButton Caffemocha;
    private JButton Espresso;
    private JButton Millecrepe;
    private JButton Montblanc;
    private JButton Hotdog;
    private JButton Toast;
    private JButton Undo;
    private JTextArea orderedItemsList;
    public int total=0;
    ArrayList<Item> itemList = new ArrayList<Item>();
    class Item{
        private String name;
        private int price;
        private String state;
        private int num;
        private String size;
        Item(String name,int price,String state){
            this.name=name;
            this.price=price;
            this.state=state;
        }
    }
    public machineUI(){
        SetButton(BlendH,"","Blend Coffee",260,"HOT",52);
        SetButton(BlendC,"","Blend Coffee",260,"COLD",52);
        SetButton(AmericanH,"","American Coffee",250,"HOT",42);
        SetButton(AmericanC,"","American Coffee",250,"COLD",42);
        SetButton(CocoaH,"","Cocoa",350,"HOT",72);
        SetButton(CocoaC,"","Cocoa",350,"COLD",72);
        SetButton(TeaH,"","Tea",240,"HOT",42);
        SetButton(TeaC,"","Tea",240,"COLD",42);
        SetButton(Caffemocha,"Caffemocha.jpg","Caffe Mocha",390,"HOT Only",51);
        SetButton(Espresso,"Espresso.jpg","Espresso",270,"HOT Only",0);
        SetButton(Millecrepe,"Millecrepe.jpg","Mille Crepe",400,"food",0);
        SetButton(Montblanc,"Montblanc.jpg","Mont Blanc",530,"food",0);
        SetButton(Hotdog,"Hotdog.jpg","Hot dog",290,"food",0);
        SetButton(Toast,"Toast.jpg","Toast",200,"food",0);

        CheckButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total==0){
                    JOptionPane.showMessageDialog(null,
                            "You didn't seem to order anything.");
                }else {
                    JOptionPane.showConfirmDialog(null,
                            "Would you like to check out?",
                            "Check Out Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    JOptionPane.showMessageDialog(null,
                            "Thank you for your order.\n The total price is ¥" + total);
                    total = 0;
                    itemList.clear();
                    UpDateList();
                }
            }
        });
        CancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(itemList.size()!=0) {
                    itemList.clear();
                    total = 0;
                    UpDateList();
                }
            }
        });
        Undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(itemList.size()!=0) {
                    itemList.remove(itemList.size() - 1);
                    UpDateList();
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("machineUI");
        frame.setContentPane(new machineUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    private void SetButton(JButton button,String imageName, String name,int price,String type,int step){//step一桁目サイズ分類二桁目値段ステップ
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Item item = new Item(name,price,type);
                int sizeNum=0;
                int num;
                String size="";
                if(step!=0){
                    sizeNum=SizeDialog(step,item);

                    num=QuantityDialog(item);
                    item.price+=(step/10)*sizeNum*10;
                    if(step%10==2){
                        size = ConvertSize(sizeNum);
                    }else{
                        size = ConvertSize(sizeNum+1);
                    }
                }else{
                    num=QuantityDialog(item);
                }
                item.size=size;
                item.num=num+1;
                if(ConfirmationDialog(item)==0){
                    itemList.add(item);
                }
                UpDateList();
            }
        });
        if (type != "HOT" || type != "COLD") {
            button.setIcon(new ImageIcon(this.getClass().getResource((imageName))));
        }
    }
    private void UpDateList(){
        orderedItemsList.setText("");
        String currentText="";
        Item item;
        total=0;
        for(int i=0;i<itemList.size();i++){
            item=itemList.get(i);
            if(item.state=="food"){
                orderedItemsList.setText(currentText+"・"+item.num+" "+item.name+"    ¥"+item.num*item.price+"\n");
            }else{
                orderedItemsList.setText(currentText+"・"+item.num+" "+item.name+" ("+item.state+") "+item.size+"     ¥"+item.num*item.price+"\n");
            }
            currentText=orderedItemsList.getText();
            total+=item.num*item.price;
        }
        SUMTextField.setText("Total : ¥"+total);
    }

    private int SizeDialog(int step,Item item){
        int sizeNum = step%10;
        if(sizeNum==2){
            String[] buttons = {"S:¥"+item.price,"M +¥"+(step/10)*10,"L +¥"+(step/10)*20};
            return JOptionPane.showOptionDialog(null,
                    "Please select the size of your choice!",
                    item.name+" ("+item.state+") size",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    buttons,buttons[0]);
        }else{
            String[] buttons = {"M:"+item.price,"L +¥"+(step/10)*10};
            return JOptionPane.showOptionDialog(null,
                    "Please select the size of your choice!",
                    item.name+" ("+item.state+") size",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    buttons,buttons[0]);
        }
    }
    private int QuantityDialog(Item item){
        String[] quantitys = {"1","2","3"};
        return JOptionPane.showOptionDialog(null,
                "How many would you like?",
                item.name+" Quantity Selection",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                quantitys,quantitys[0]);
    }
    private String ConvertSize(int size){
        String[] sizes = {"S","M","L"};
        return sizes[size];
    }
    private int ConfirmationDialog(Item item){
        int confirmation;
        if(item.state!="food"){
            if(item.size!=""){
                confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order "+item.num+" "+item.name+" ("+item.state+") ?\nSize:"+item.size+"     Price: ¥"+item.price*item.num,
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
            }else {
                confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order " +item.num+" "+ item.name + " (" + item.state + ") ?\nPrice: ¥"+item.num*item.price,
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
            }
        }else{
            confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to order " +item.num+" "+ item.name + " ?\nPrice:"+item.num*item.price,
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
        }
        return confirmation;
    }
}
